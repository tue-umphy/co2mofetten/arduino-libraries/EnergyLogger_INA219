#include <EnergyLogger_INA219.h>

EnergyLogger_INA219 EnergyLogger;

void setup()
{
  Serial.begin(9600);
  EnergyLogger.begin();
  EnergyLogger.setCalibration_16V_400mA();
  while(not Serial); // wait for serial interface
  Serial.println("Setup finished");Serial.flush();
  EnergyLogger.init_energy_logging();
}

void loop()
{
  EnergyLogger.log_energy();
  Serial.print("Energy consumption: [J] ");
    Serial.println(EnergyLogger.total_energy_J);
  Serial.print("Energy consumption: [Wh] ");
    Serial.println(EnergyLogger.total_energy_Wh);
  Serial.print("Average Energy consumption: [mW] ");
    Serial.println(EnergyLogger.average_power_mW());
  delay(1000);
}

